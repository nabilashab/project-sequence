# Project Versions

| SL No. | Project Name | Version (Build) | Status |
| ------ | ------------ | --------------- | ------ |
| 1.     | G4Max Dialer | 1.2 (10)        | P      |
| 2.     | Mobimint     | 1.0 (1)         | WR     |
|        |              |                 |        |

